package mc.spigot.plugin.villagernames;

import org.bukkit.plugin.java.JavaPlugin;

import mc.spigot.plugin.villagernames.listener.CreatureSpawnListener;
import mc.spigot.plugin.villagernames.listener.PlayerInteractEntityListener;

public class SpigotPluginVillagerNames extends JavaPlugin {
	@Override
	public void onEnable() {
		super.onEnable();
		getLogger().info("Registering Listener for Creature-Spawnings ...");
		getServer().getPluginManager().registerEvents(new CreatureSpawnListener(this), this);
		getLogger().info("Registering Listener for Player-Interactions with Entities ...");
		getServer().getPluginManager().registerEvents(new PlayerInteractEntityListener(), this);
	}
}
