package mc.spigot.plugin.villagernames.listener;

import org.bukkit.Color;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.plugin.Plugin;

import mc.spigot.plugin.utils.PlayerUtil;
import mc.spigot.plugin.utils.VillagerUtil;

public class CreatureSpawnListener implements Listener {
	private final Plugin plugin;

	public CreatureSpawnListener(final Plugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if (EntityType.VILLAGER.equals(event.getEntity().getType())) {
			final Villager villager = (Villager) event.getEntity();

			if (villager.getProfession().isZombie()) {
				// Don't give those zombies a name! We don't want to get personal with them!
				return;
			}

			plugin.getLogger().info(
					"A new villager has been spawned: " + villager.getName() + " (" + villager.getEntityId() + ")");

			final String customName = VillagerUtil.getCustomNameByJob(villager) + "_" + villager.getEntityId();

			villager.setCustomName(customName);
			villager.setCustomNameVisible(true);
			plugin.getLogger().info("The villager " + villager.getEntityId() + " with profession '"
					+ villager.getProfession() + "' has been renamed to: " + customName);

			PlayerUtil.informAllPlayers(plugin,
					Color.GRAY + "Ein neuer Dorfbewohner wurde geboren: " + villager.getCustomName());
		}
	}
}