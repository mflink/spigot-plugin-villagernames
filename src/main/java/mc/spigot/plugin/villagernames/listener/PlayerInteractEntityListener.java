package mc.spigot.plugin.villagernames.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import mc.spigot.plugin.utils.VillagerUtil;

public class PlayerInteractEntityListener implements Listener {
	@EventHandler
	public void onPlayerInteractEntityEvent(final PlayerInteractEntityEvent event) {
		if (EntityType.VILLAGER.equals(event.getRightClicked().getType())) {
			final Villager villager = (Villager) event.getRightClicked();

			//if (villager.getCustomName() == null || villager.getCustomName().isEmpty()) {
				// Only set name, if not set (manual or automatic) already.

				if (villager.getProfession().isZombie()) {
					// Don't give those zombies a name! We don't want to get personal with them!
					return;
				}

				final String customName = VillagerUtil.getCustomNameByJob(villager) + "_" + villager.getEntityId();

				villager.setCustomName(customName);
				villager.setCustomNameVisible(true);
			//}
		}
	}
}
