package mc.spigot.plugin.utils;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;

public class VillagerUtil {
	public static final String BUTCHER_LEATHERWORKER = "Gerber";
	public static final String JOBNAME_FARMER_FARMER = "Bauer";
	public static final String JOBNAME_FARMER_FISHER = "Fischer";
	public static final String JOBNAME_FARMER_SHEPHERD = "Schäfer";
	public static final String JOBNAME_FARMER_FLETCHER = "Pfeilmacher";
	public static final String JOBNAME_LIBRARIAN_LIBRARIAN = "Bibliothekar";
	public static final String JOBNAME_LIBRARIAN_CARTOGRAPHER = "Kartograph";
	public static final String JOBNAME_PRIEST_CLERIC = "Geistlicher";
	public static final String JOBNAME_BLACKSMITH_ARMORER = "Rüstungsschmied";
	public static final String JOBNAME_BLACKSMITH_WEAPONSMITH = "Waffenschmied";
	public static final String JOBNAME_BLACKSMITH_TOOLSMITH = "Werkzeugschmied";
	public static final String JOBNAME_BLACKSMITH_BLACKSMITH = "Schmied";
	public static final String JOBNAME_BUTCHER_BUTCHER = "Fleischer";
	public static final String JOBNAME_NITWIT_NITWIT = "Nichtsnutz";

	public static String getCustomNameByJob(final Villager villager) {
		String customName = null;
		final Set<Material> recipeResults = new HashSet<>();
		final Set<Material> recipeIngredients = new HashSet<>();

		for (final MerchantRecipe recipe : villager.getRecipes()) {
			recipeResults.add(recipe.getResult().getType());

			if (recipe.getIngredients() != null) {
				for (final ItemStack ingredient : recipe.getIngredients())
					recipeIngredients.add(ingredient.getType());
			}
		}

		switch (villager.getProfession()) {
		case FARMER:
			if (recipeResults.contains(Material.BREAD)) {
				customName = JOBNAME_FARMER_FARMER;
			} else if (recipeResults.contains(Material.COOKED_FISH)) {
				customName = JOBNAME_FARMER_FISHER;
			} else if (recipeResults.contains(Material.SHEARS)) {
				customName = JOBNAME_FARMER_SHEPHERD;
			} else if (recipeResults.contains(Material.ARROW)) {
				customName = JOBNAME_FARMER_FLETCHER;
			} else {
				// Oh! A new or custom career! Exciting!
				customName = JOBNAME_FARMER_FARMER;
			}
			break;
		case LIBRARIAN:
			if (recipeResults.contains(Material.WRITTEN_BOOK)) {
				customName = JOBNAME_LIBRARIAN_LIBRARIAN;
			} else if (recipeIngredients.contains(Material.COMPASS)) {
				customName = JOBNAME_LIBRARIAN_CARTOGRAPHER;
			} else {
				// Oh! A new or custom career! Exciting!
				customName = JOBNAME_LIBRARIAN_LIBRARIAN;
			}
			break;
		case PRIEST:
			customName = JOBNAME_PRIEST_CLERIC;
			break;
		case BLACKSMITH:
			if (recipeResults.contains(Material.IRON_HELMET)) {
				customName = JOBNAME_BLACKSMITH_ARMORER;
			} else if (recipeResults.contains(Material.IRON_AXE)) {
				customName = JOBNAME_BLACKSMITH_WEAPONSMITH;
			} else if (recipeResults.contains(Material.IRON_SPADE)) {
				customName = JOBNAME_BLACKSMITH_TOOLSMITH;
			} else {
				// Oh! A new or custom career! Exciting! Let's show his first item.
				customName = JOBNAME_BLACKSMITH_BLACKSMITH;
			}
			break;
		case BUTCHER:
			if (recipeResults.contains(Material.GRILLED_PORK)) {
				customName = JOBNAME_BUTCHER_BUTCHER;
			} else if (recipeResults.contains(Material.LEATHER_LEGGINGS)) {
				customName = BUTCHER_LEATHERWORKER;
			} else {
				// Oh! A new or custom career! Exciting! Let's show his first item.
				customName = JOBNAME_BUTCHER_BUTCHER;
			}
			break;
		case NITWIT:
			customName = JOBNAME_NITWIT_NITWIT;
			break;
		default:
			// Oh! We don't know the profession of this guy by now. Let's show his profession instead.
			customName = villager.getProfession().name();
			break;
		}

		if (recipeResults.isEmpty() && !villager.getProfession().equals(Profession.NITWIT)) {
			// This guy has nothing to sell, but got a profession other than Nitwit?! Let's treat him like a special Nitwit. 
			customName = JOBNAME_NITWIT_NITWIT + " (" + customName + ")";
		}

		return customName;
	}
}
