package mc.spigot.plugin.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class PlayerUtil {
	public static void informAllPlayers(final Plugin plugin, final String message) {
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()) {
					player.sendMessage(message);
				}
			}

		}, 20);
	}
}
