# Overview
This [Spigot](https://www.spigotmc.org/wiki/spigot/)-Plugin shows the name of a villager above his head depending on his job.
It will be assigned on first use or spawn of a specific villager.

# Description
Currently the names are hard-coded (see [Todo](#Todo)) and in german.
The names will be set as follows:
<table>
	<thead>
		<tr>
			<td colspan="2" style="text-align: center">Criteria</td>
			<td style="text-align: center">Result</td>
		</tr>
		<tr>
			<th>Profession</th>
			<th>Recipe-results contains</th>
			<th>CustomName</th>
		</tr>
	</thead>
	<tbody>
		<!-- Farmer -->
		<tr>
			<td>Farmer</td>
			<td>Bread</td>
			<td>Bauer</td>
		</tr>
		<tr>
			<td>Farmer</td>
			<td>Cooked fish</td>
			<td>Fischer</td>
		</tr>
		<tr>
			<td>Farmer</td>
			<td>Shears</td>
			<td>Sch&auml;fer</td>
		</tr>
		<tr>
			<td>Farmer</td>
			<td>Arrow</td>
			<td>Pfeilmacher</td>
		</tr>
		<tr>
			<td>Farmer</td>
			<td>- nothing from above -</td>
			<td>Bauer</td>
		</tr>
		<!-- Librarian -->
		<tr>
			<td>Librarian</td>
			<td>Written book</td>
			<td>Bibliothekar</td>
		</tr>
		<tr>
			<td>Librarian</td>
			<td>Map<sup><a href="#note1">[1]</a></sup></td>
			<td>Kartograph</td>
		</tr>
		<tr>
			<td>Librarian</td>
			<td>- nothing from above -</td>
			<td>Bibliothekar</td>
		</tr>
		<!-- Priest -->
		<tr>
			<td>Priest</td>
			<td>- does not matter -</td>
			<td>Geistlicher</td>
		</tr>
		<!-- Blacksmith -->
		<tr>
			<td>Blacksmith</td>
			<td>Iron helmet</td>
			<td>R&uuml;stungsschmied</td>
		</tr>
		<tr>
			<td>Blacksmith</td>
			<td>Iron axe</td>
			<td>Waffenschmied</td>
		</tr>
		<tr>
			<td>Blacksmith</td>
			<td>Iron spade</td>
			<td>Werkzeugschmied</td>
		</tr>
		<tr>
			<td>Blacksmith</td>
			<td>- nothing from above -</td>
			<td>Schmied</td>
		</tr>
		<!-- Butcher -->
		<tr>
			<td>Butcher</td>
			<td>Grilled pork</td>
			<td>Fleischer</td>
		</tr>
		<tr>
			<td>Butcher</td>
			<td>Leather leggings</td>
			<td>Gerber</td>
		</tr>
		<tr>
			<td>Butcher</td>
			<td>- nothing from above -</td>
			<td>Fleischer</td>
		</tr>
		<!-- Nitwit -->
		<tr>
			<td>Nitwit</td>
			<td>- does not matter -</td>
			<td>Nichtsnutz</td>
		</tr>
		<!-- others -->
		<tr>
			<td>- unknown -</td>
			<td>- does not matter -</td>
			<td>- his profession -</td>
		</tr>
		<tr>
			<td>- one from above -</td>
			<td>- nothing -</td>
			<td>Nichtsnutz (- his detected CustomName -)</td>
		</tr>
	</tbody>
</table>

<a id="note1"></a>1: Does not work properly at the moment (see [Known bugs](#KnownBugs)). 

# Installation
* Place the resulting jar-file in the plugins-folder of your Spigot-Installation.

# Commands
This mod does not have any commands. It just works. ;-)

# <a id="Todo"></a>Todo
* Externalize the current static configuration in VillagerUtil or (better) find those strings in the spigot-code and use it.
* Create an external configuration for: names (if not in spigot-code or for customization), posting of a villagers birth/spawn in chat, showing entity-id, showing the names always or just like normal (on nearby), update the name once or always, localization of names

# <a id="KnownBugs"></a>Known bugs
1. The cartographer will be identified as a librarian, if he is in levels 1 and 2, because the map is available at cartographer level 3. We need to check the accepted items on buy here.